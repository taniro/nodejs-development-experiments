import Vue from 'vue'
import Router from 'vue-router'

import body from './vue/body.vue'

const logger = require('./logger').withPrefix('[router]');

logger.log("loaded body:", body)

Vue.use(Router)

export function createRouter () {

  logger.log("creating new router");

  return new Router({
    mode: 'history',
    routes: [
      // ...
      { path:'*', component: body }
    ]
  })

}