///
/// Simple summing up method
function sum(a, b) {
  return a + b;
}
module.exports = sum;