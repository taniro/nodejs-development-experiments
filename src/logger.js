let Logger = {
  /// Parse to preserve the given object status for logging.
  o: function(o) {
    return JSON.parse(JSON.stringify(o));
  },
  /// Primarily used by log, warn, error functions of console object.
  /// Returning a function that will merge prefix to first string argument, or prepend it as first argument of the callee.
  // functionArgumentPrefixMerge: function(prefix, fn, caller) {
  //   return function() {

  //     var args = [prefix],
  //         i = 0
  //     ;

  //     /// Merge first argument if it's a string.
  //     if( typeof arguments[i] === "string" ) {

  //       args[0] += " " + arguments[i];
  //       i++;

  //     }

  //     let lim = arguments.length;
  //     for(; i<lim ; i++) {
  //       args.push(arguments[i]);
  //     }

  //     fn.apply(caller, args);

  //   };
  // },
  /// Logger constructor, usage:
  ///   let logger = App.loggerWithPrefix("[prefix]");
  ///   logger.log("message"); // "[prefix] message"
  /// Lineage logger can be created from the parent logger. Just
  ///   let lin = log.lin("[method]");
  ///   lin.log("called"); // "[prefix][method] called"
  withPrefix: function(prefix) {

    var O = {};

    Object.defineProperty(O, "lin", {
        get: function() {
            return function(mid) { return Logger.withPrefix(prefix+mid); };
        },
        configurable: false,
        enumerable: false
    });

    for (var m in console) {
      let fn = console[m];
      if (typeof fn == 'function') {

        Object.defineProperty(O, m, {
            get: function() {
                return fn.bind(console, prefix);
            },
            configurable: false,
            enumerable: false
        });

      }
    }

    return O;

  }
};

module.exports = Logger
