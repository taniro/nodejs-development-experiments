const logger = require('./logger').withPrefix('[entry-server]');

import { createVueRoot } from './vueroot'

export let createApp = context => {

    // since there could potentially be asynchronous route hooks or components,
    // we will be returning a Promise so that the server can wait until
    // everything is ready before rendering.
    return new Promise((resolve, reject) => {

        const { app, router } = createVueRoot(context)

        router.push(context.url)

        router.onReady(() => {

            const matchedComponents = router.getMatchedComponents()

            // no matched routes, reject with 404
            if (!matchedComponents.length) {
              return reject({ code: 404 })
            }

            logger.log("matched components", matchedComponents)

            // the Promise should resolve to the app instance so it can be rendered
            resolve(app)

        }, reject)

    })

}