const logger = require('./logger').withPrefix('[entry-client]');

import { createVueRoot } from './vueroot'

// client-specific bootstrapping logic...

const { app, router } = createVueRoot()

logger.log("app init...", app, router)

router.onReady(()=>{

  logger.log("app", arguments, app.$mount)

  // this assumes template root element has `id="app"`
  app.$mount('#app', true)

  logger.log("app mounted?")

})
