import body from './vue/body.vue'
import Vue from 'vue'
import { createRouter } from './router'

export function createVueRoot (context) {

    console.log("context:", context);

    const router = createRouter()

    const app = new Vue({
        router,
        render: h => h(body, { props: { url: context.url } })
    })

    return { app, router }

}