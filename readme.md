v0.01, 2019 April

# Description

An experimental project that explores possible development options and
configurations.

# Installation

## Configure .env

Database connection is configured through environment variables, which
should be set in .env and loaded by dotenv.

``` bash
PSQL_DB=postgres://user:password@localhost/database_name
```

> **Note**
> 
> Considering security and implementation concerns of environment
> variables, we may need to find another solution. Possible: - Configure
> database and password at installation process, then store those in an
> encrypted file.

# Code And Runtime Notes

## dotenv environment variables

The code use dotenv to load environment variables from .env so that
sensitive data can be separated from code. One of the example is
database connection data.

# Database

## SQL Commands

Table definitions are store in base.sql.

# Files And Paths

## Sequelize.js files and folders

  - config/database.config.json `Database config file, configured as
    receiving connection data from environment variables. See .env
    configure section for detail.`

  - models `Generated models.`

  - seeders

  - migrations

## Rebuild Models using Sequelize.js

> **Note**
> 
> TODO: This section needs further work.
