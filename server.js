require('dotenv').config()

const logger = require('./src/logger').withPrefix('[server]');

const isProd = false

const Vue = require('vue')
const express = require('express')
const server = express()

// Vue SSR
const { createRenderer } = require('vue-server-renderer')
const renderer = createRenderer({
        template: require('fs').readFileSync('./site/index.html', 'utf-8')
      })

const { createApp } = require('./dist/server.bundle')


server.get(/^\/(?!r\/).*/, (req, res) => {

  const lin = logger.lin("[get]")

  lin.log("url:", req.url)

  const context = { url: req.url }

  createApp(context).then( app => {

        renderer.renderToString(app, (err, html) => {

            const lin = logger.lin("<resolve>")

            if (err) {
                if (err.code === 404) {
                    res.status(404).end('Page not found')
                } else {
                    res.status(500).end('Internal Server Error')
                }
            } else {

                res.end(html)
                // lin.log("[html]:", html)

            }

        })

  } ).catch(() => {

        const lin = logger.lin("<rejected>")

        lin.log("arguments", arguments)

        res.status(500).end('Internal Server Error')

  })



})

const path = require('path')
const resolve = file => path.resolve(__dirname, file)
const serve = (path, cache) => express.static(resolve(path), {
  maxAge: cache && isProd ? 1000 * 60 * 60 * 24 * 30 : 0
})

server.use('/r', serve('./site'))



server.listen(1233)
