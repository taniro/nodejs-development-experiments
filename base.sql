CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    name text,
    alias_name text,
    email text,
    validated boolean,
    secret text,
    apply_date date
);