/* jshint indent: 1 */

module.exports = function(sequelize, DataTypes) {
	return sequelize.define('users', {
		id: {
			type: DataTypes.INTEGER,
			allowNull: false,
			primaryKey: true,
			autoIncrement: true
		},
		name: {
			type: DataTypes.TEXT,
			allowNull: true
		},
		alias_name: {
			type: DataTypes.TEXT,
			allowNull: true
		},
		email: {
			type: DataTypes.TEXT,
			allowNull: true
		},
		validated: {
			type: DataTypes.BOOLEAN,
			allowNull: true
		},
		secret: {
			type: DataTypes.TEXT,
			allowNull: true
		}
	}, {
		tableName: 'users',
        timestamps: false
	}
	);
};
