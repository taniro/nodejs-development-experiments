DOCUMENTS = readme

# Compile both server and client scripts
build:
	npm run build

# Compile server-side script
server:
	npm run build:server

# Compile client-side script
client:
	npm run build:client

# Run unit tests
test:
	jest

# Compile project documents
doc: $(DOCUMENTS)

$(DOCUMENTS): % : %.adoc
	asciidoctor -r asciidoctor-diagram $<
	asciidoctor -b docbook $< -o - | pandoc -f docbook -t gfm -o $@.md
