const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge')
const base = require('./webpack.config.base')

const config = merge(base, {
    entry: './src/entry-client.js',
    output: {
        filename: 'client.bundle.js'
    }
});

module.exports = config