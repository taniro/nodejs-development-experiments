// Server side bundle
const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge')
const base = require('./webpack.config.base')

const config = merge(base, {
    target: "node",
    entry: './src/entry-server.js',
    output: {
        filename: 'server.bundle.js',
        libraryTarget: "commonjs2"
    }
})

module.exports = config